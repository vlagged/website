---
title: Statutes
date: 2022-12-10 16:56:54
categories: verein
keywords:
---

This is the Status document for the Austrian Association:

[Statuten_des_Verein_Sailmates.pdf](Statuten_des_Verein_Sailmates.pdf)

The ZVR number is 1967552494

And you can find our official registration here:
https://citizen.bmi.gv.at/at.gv.bmi.fnsweb-p/zvn/public/Registerauszug