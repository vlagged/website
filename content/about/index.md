---
title: About
date: 2022-12-08 22:00:00
---

Sailmates is a non-profit association who's objective is to support and promote free and open source mobile operating system alternatives.

See also the [statute](/statutes) document.

