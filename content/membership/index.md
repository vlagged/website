---
title: Membership
date: 2022-12-10 16:56:54
categories: verein
keywords:
---

Currently to become a member you need to:

1. Have access to [Telegram](https://telegram.org) so that you can contact us to provide your telegram username to join the Sailmates group.
We are available as [b100dian](https://t.me/b100dian), [fridlmue](https://t.me/fridlmue) and [mc_lemon](https://t.me/mc_lemon)

2. Pay the 1 year membership of 42EUR or 3.5EUR/month to
```
Account holder: Sailmates Förderverein für freie
BIC: TRWIBEB1XXX
IBAN: BE65 9674 8601 2196
Wise's address: Avenue Louise 54, Room S52
Brussels
1050
Belgium
```